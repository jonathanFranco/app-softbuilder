import React, { Component } from 'react';
import Header from './components/Header';
import PessoaForm from './components/Pessoas';

class App extends Component {

  render() {

    return (
      <div className="App container my-4">
        <Header title="App Cadastro" />
        <PessoaForm />
      </div>

    );
  }
}

export default App;