import React, { Component } from 'react';
import Axios from 'axios';

class PessoaForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            nome: '',
            email: '',
            cadastro: []
        }
    }
    componentDidMount() {
        this.getAll();
    }
    getAll() {
        Axios.get(`http://localhost/api-crud/server/public/api/cadastro`)
            .then((res) => {
                this.setState({
                    cadastro: res.data,
                    id: 0,
                    nome: '',
                    email: '',
                })
            })
    }
    getOne(cadastro) {
        this.setState({
            id: cadastro.id,
            nome: cadastro.nome,
            email: cadastro.email
        })
    }
    delete(id) {
        Axios.delete(`http://localhost/api-crud/server/public/api/cadastro/${id}`)
            .then((res) => {
                this.getAll();
            })
    }
    submit(event, id) {
        event.preventDefault();
        if (this.state.id == 0) {
            Axios.post(`http://localhost/api-crud/server/public/api/cadastro`, { nome: this.state.nome, email: this.state.email })
                .then((res) => {
                    this.getAll();
                })
        } else {
            Axios.put(`http://localhost/api-crud/server/public/api/cadastro/${id}`, { nome: this.state.nome, email: this.state.email })
                .then((res) => {
                    this.getAll();
                })
        }

    }
    nomechange(event) {
        this.setState({
            nome: event.target.value
        })
    }
    emailchange(event) {
        this.setState({
            email: event.target.value
        })
    }
    render() {
        return (
            <div className="container pt-2">

                <div className="row">
                    <div className="col-md-4">
                        <h3 className="text-success text-left px-3 font-weight-bold">Cadastro de Pessoas</h3>
                        <hr className="ml-3 success-color mb-4 w-25 mt-0"></hr>
                        <form onSubmit="window.location.reload()" onSubmit={(e) => this.submit(e, this.state.id)}>
                            <div className="form-group text-left px-3 pb-1 text-left">
                                <input maxLength="40" type="text" className=" form-control form-control-md bg-light" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome" onChange={(e) => this.nomechange(e)} value={this.state.nome} name="primeiro-nome" required />
                                <label className="small grey-text mt-2">* Digite só o primeiro nome</label>
                            </div>
                            <div className="form-group text-left px-3">
                                <input maxLength="40" type="email" className="form-control form-control-md bg-light" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="exemplo@email.com" onChange={(e) => this.emailchange(e)} value={this.state.email} name="e-mail" required />
                            </div>
                            <div className="mb-5 mt-5 px-3">
                                <button type="submit" className="btn btn-success btn-block small"><i className="fas fa-save mr-2"></i> Salvar Alterações</button>
                            </div>
                        </form>
                    </div>

                    <div className="col-md-8 text-center">
                        <table className="table table-hover white table-bordered">
                            <thead className="success-color">
                                <tr>
                                    <th className="text-white" scope="col"><i className="fas fa-user-alt mr-2"></i>Nome</th>
                                    <th className="text-white" scope="col"><i className="fas fa-envelope mr-2"></i>Email</th>
                                    <th className="text-white" scope="col">Opções<i className="fas fa-ellipsis-v ml-2"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.cadastro.map(cadastro =>
                                    <tr key={cadastro.id}>
                                        <td>{cadastro.nome}</td>
                                        <td>{cadastro.email}</td>
                                        <td>
                                            <button onClick={(e) => this.getOne(cadastro)} className=" mr-2 bg-transparent border-0 z-depth-0 ">
                                                <i className="fas fa-pen text-dark h4-responsive"></i>
                                            </button>
                                            <button onClick={(e) => this.delete(cadastro.id)} className="bg-transparent border-0 z-depth-0">
                                                <i className="fas fa-trash-alt text-danger h4-responsive"></i>
                                            </button>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        );
    }
}
export default PessoaForm;