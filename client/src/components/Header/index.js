import React from 'react';

const Header = ({ title }) => (
    <header className="mb-5">
        <h1 className="font-weight-bold text-dark"><img src="LogoBranca.png" className="w-25" /> </h1>
    </header>

);

export default Header;